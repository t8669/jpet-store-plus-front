import { createRouter, createWebHistory } from 'vue-router'

import Login from '@/views/login/index.vue'
import Layout from '@/views/layout/index.vue'
import Home from '@/views/layout/home.vue'
import Categroy from '@/views/layout/category.vue'
import Cart from '@/views/layout/cart.vue'
import My from '@/views/layout/my.vue'
import Search from '@/views/search/index.vue'
import Myorder from '@/views/myorder/index.vue'
import Pay from '@/views/pay/index.vue'
import Prodetail from '@/views/prodetail/index.vue'
import SearchList from '@/views/search/list.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/login', component: Login },
    {
      path: '/',
      component: Layout,
      redirect: '/home',
      children: [
        { path: '/home', component: Home },
        { path: '/categroy', component: Categroy },
        { path: '/cart', component: Cart },
        { path: '/my', component: My }
      ]
    },
    { path: '/search', component: Search },
    { path: '/myorder', component: Myorder },
    { path: '/pay', component: Pay },
    { path: '/prodetail/:id', component: Prodetail },
    { path: '/searchlist/:key', component: SearchList },
]
})

export default router
