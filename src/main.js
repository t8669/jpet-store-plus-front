
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createPersistedState } from 'pinia-persistedstate-plugin'
import { SwipeCell, PullRefresh, showToast} from 'vant'
import App from './App.vue'
import router from './router'


const app = createApp(App)
const pinia = createPinia();
const persist = createPersistedState();
pinia.use(persist)
app.use(pinia)
app.use(router)
app.use(SwipeCell)
app.use(PullRefresh)
app.use(showToast)

app.mount('#app')
