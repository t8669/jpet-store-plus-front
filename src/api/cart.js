import request from '@/utils/request.js'
import { useTokenStore } from '@/stores/token.js';

//获取购物车信息
export const getCartItemListService = ()=>{
    return request.get('/carts')
}
//删除购物车
export const deleteCartService = ()=>{
    return request.delete('/carts')
}
//添加商品到购物车
export const addCartItemService= (itemId, quantity)=>{
    const params = new URLSearchParams();
    params.append('itemId', itemId);
    params.append('quantity', quantity);
    return request.post('/cartItems', params);
}
//更新购物车商品数目
export const updateCartItemService= (itemId, quantity)=>{
    const params = new URLSearchParams();
    params.append('itemId', itemId);
    params.append('quantity', quantity);
    return request.put('/cartItems', params);
}
