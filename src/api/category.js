import request from '@/utils/request.js'
import { useTokenStore } from '@/stores/token.js';

//商品大类
export const categoryListService = ()=>{
   return request.get('/categories')
}

//所有商品
export const allProductListService =()=>{
    return request.get('/products')
}

//根据category的id获取products
export const productByCategoryService=(id)=>{
    const url = `/categories/${id}/products`
    return request.get(url)
}

//根据product的id获取items
export const itemsByProductService=(id)=>{
    const url = `/products/${id}/items`
    return request.get(url)
}