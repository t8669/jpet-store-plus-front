import request from '@/utils/request';
import { useTokenStore } from '@/stores/token';

const userService = (url, username, password) => {
    const params = new URLSearchParams()
    params.append('username', username)
    params.append('password', password)
    return request.post(url, params);
};

export const userLoginService = (username, password) => {
    return userService('/tokens/login', username, password);
};

export const userRegisterService = (username, password) => {
    return userService('/accounts/register', username, password);
};

export const userLogoutService = () => {
    return request.delete('/tokens/logout', { headers: { 'Authorization': useTokenStore().token } });
};

export const userGetProfileService = () => {
    return request.get('/accounts', { headers: { 'Authorization': useTokenStore().token } });
};

export const userUpdateProfileService = (data) => {
    return request.put('/accounts', data, { headers: { 'Authorization': useTokenStore().token } });
};

export const userChangePasswordService = (oldPassword, newPassword) => {
    return request.patch('/accounts', { oldPassword, newPassword }, { headers: { 'Authorization': useTokenStore().token } });
};

export const getOrdersService = () => {
    return request.get('/accounts/orders', { headers: { 'Authorization': useTokenStore().token } });
};