import request from '@/utils/request.js'
import { useTokenStore } from '@/stores/token.js';
//创建订单
export const createOrderService= async (order) => {
    return request.post('/orders', order, { headers: { 'Authorization': `Bearer ${useTokenStore().token}` } });
}
//获取订单
export const getOrderService= (orderId)=>{
    return request.get(`/orders/${orderId}`,{ headers: { 'Authorization': useTokenStore().token } })
}