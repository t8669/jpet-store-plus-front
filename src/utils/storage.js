//搜索记录持久化到本地
const HISTORY_KEY = 'history_list'
const CART_KEY = 'cart_list'

// 获取搜索历史
export const getHistoryList = () => {
  const result = localStorage.getItem(HISTORY_KEY)
  return result ? JSON.parse(result) : []
}

// 设置搜索历史
export const setHistoryList = (arr) => {
  localStorage.setItem(HISTORY_KEY, JSON.stringify(arr))
}

// 获取购物车选中
export const getCartList = () => {
  const result = localStorage.getItem(CART_KEY)
  return result? JSON.parse(result) : []
}

// 设置购物车选中
export const setCartList = (arr) => {
  localStorage.setItem(CART_KEY, JSON.stringify(arr))
}