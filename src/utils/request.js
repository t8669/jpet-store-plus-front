//定制请求的实例

//导入css
import 'vant/es/toast/style'
//导入axios  npm install axios
import axios from 'axios';
//js文件中导入router的方法
import router from '@/router'
//导入token
import { useTokenStore } from '@/stores/token.js';
//组件库消息提示
import { showSuccessToast, showFailToast } from 'vant';
//定义一个变量,记录公共的前缀  ,  baseURL
const baseURL = '/api';
const instance = axios.create({baseURL})


//添加响应拦截器
instance.interceptors.response.use(
    result=>{
        //注意：判断条件要正确，statusCode==='0'则成功
        if(result.data.statusCode===0){ 
            return result;         
        }
       //alert(result.message?result.message:'服务异常')
        if (result.data.message === '' || result.data.message === null || result.data.message === undefined) {
            showFailToast({
                message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-fail" style="font-size:16px;margin-right:5px"></i><span>当前系统繁忙，请稍后再试</span></div>`,
                type: 'html'
            });
        } else {
            showFailToast({
                message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-fail" style="font-size:16px;margin-right:5px"></i><span>` + result.data.message + `</span></div>`,
                type: 'html'
            });
        }
       console.log(result)
       return Promise.reject(result.data)
    },
    err=>{
        //401未登录
        console.log(err)
        if (err.response.status === 401) {
            showFailToast({
                message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-fail" style="font-size:16px;margin-right:5px"></i><span>请先登录</span></div>`,
                type: 'html'
            });
            router.push('/login')
        } else {
            if (err.response.message === ''|| err.response.message === null || err.response.message === undefined) {
                showFailToast({
                    message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-fail" style="font-size:16px;margin-right:5px"></i><span>当前系统繁忙，请稍后再试</span></div>`,
                    type: 'html'
                });
            } else {
                showFailToast({
                    message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-fail" style="font-size:16px;margin-right:5px"></i><span>` + err.response.data.message + `</span></div>`,
                    type: 'html'
                });
            }
        }
        
        return Promise.reject(err);//异步的状态转化成失败的状态
    }
)

//添加请求拦截器
instance.interceptors.request.use(
    (config)=>{
        console.log('正在请求路径：' + config.url);
        if (config.url === '/admins/register') {
            console.log('请求路径为注册页，无需设置请求头');
            return config;
        } else {
            const tokenStore = useTokenStore();
            if (tokenStore.token)
                config.headers.Authorization = tokenStore.token;
                if(config.url === '/tokens/login') {
                    showSuccessToast({
                        message: `<div style="height:22px;display:flex;justify-content: center;align-items: center;user-select:none;"><i class="van-icon van-icon-success" style="font-size:16px;margin-right:5px"></i><span>登录成功</span></div>`,
                        type: 'html'
                    });
                }
            else
                console.log('Token为空, 未设置请求头')
            return config;
        }
    },
    (err)=>{
        console.log(err)
        Promise.reject(err)
    }
)

export default instance;